package com.huutho.movies

import android.content.Context
import com.google.gson.Gson
import com.huutho.baselibrary.BaseSharePref

class AppSharePref(private val context: Context,
                   private val nameOfSharePref: String,
                   private val gson: Gson
) : BaseSharePref(context, nameOfSharePref, gson)