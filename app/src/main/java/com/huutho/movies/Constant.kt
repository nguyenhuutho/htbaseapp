package com.huutho.movies

object Constant {

    // https://developers.themoviedb.org/3/configuration/get-api-configuration
    const val IMG_BASE_URL = "http://image.tmdb.org/t/p/"
    const val IMG_S_BASE_URL = "https://image.tmdb.org/t/p/"
    const val IMG_BACKDROP_SIZE = "original"
    const val IMG_LOGO_SIZE = "original"
    const val IMG_POSTER_SIZE = "original"
    const val IMG_PROFILE_SIZE = "original"
    const val IMG_STILL_SIZE = "original"
}