package com.huutho.movies.utils

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.huutho.movies.Constant

@BindingAdapter("app:setBackdropImage")
fun setBackdropImage(view: ImageView, url: String) {
    Glide
        .with(view.context)
        .load(generateImageUrl(Constant.IMG_BACKDROP_SIZE, url))
        .diskCacheStrategy(DiskCacheStrategy.ALL)
        .into(view)
}

@BindingAdapter("app:setLogoImage")
fun setLogoImage(view: ImageView, url: String) {
    Glide
        .with(view.context)
        .load(generateImageUrl(Constant.IMG_LOGO_SIZE, url))
        .diskCacheStrategy(DiskCacheStrategy.ALL)
        .into(view)
}

@BindingAdapter("app:setPosterImage")
fun setPosterImage(view: ImageView, url: String) {
    Glide
        .with(view.context)
        .load(generateImageUrl(Constant.IMG_POSTER_SIZE, url))
        .diskCacheStrategy(DiskCacheStrategy.ALL)
        .into(view)
}


private fun generateImageUrl(size: String, url: String): String {
    return if (url.contains("http")) url
    else "${Constant.IMG_S_BASE_URL}$size$url"
}