package com.huutho.movies.main.fragments.discovers

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import androidx.annotation.IdRes
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.chip.ChipGroup
import com.huutho.baselibrary.BaseFragment
import com.huutho.baselibrary.data.handleDataDefault
import com.huutho.movies.GenresID
import com.huutho.movies.R
import com.huutho.movies.databinding.FragmentDiscorverBinding
import com.huutho.movies.entity.Movie
import com.huutho.movies.entity.MoviesByCategoryRequest
import com.jcodecraeer.xrecyclerview.ProgressStyle
import com.jcodecraeer.xrecyclerview.XRecyclerView
import org.koin.androidx.viewmodel.ext.android.viewModel

@SuppressLint("InflateParams")
class DiscoverFragment : BaseFragment<FragmentDiscorverBinding, DiscoverVM>() {

    private var page: Int = 1

    private var lastIdChecked: Int = 0

    private val headerView: ChipGroup by lazy {
        val group = LayoutInflater.from(context)
            .inflate(R.layout.header_discover, null, false) as ChipGroup
        group.setOnCheckedChangeListener { _, chipId ->
            if (lastIdChecked != chipId) {
                page = 1
                fetchData(chipId)
                adapter.clear()
                lastIdChecked = chipId
            }
        }
        group
    }

    private val adapter = DiscoveryMoviesAdapter { data, position ->

    }

    override val viewModel: DiscoverVM by viewModel()

    override fun getLayoutId(): Int = R.layout.fragment_discorver

    override fun setBindingForLayout(binding: FragmentDiscorverBinding) {
        binding.vm = viewModel
    }

    override fun onViewReady(view: View) {
        initView()
    }

    override fun onObserverListener(viewModel: DiscoverVM) {
        viewModel.moviesByGenreId.observe(this, Observer { response ->
            handleDataDefault(response,
                onSuccess = {
                    handleData(it?.results)
                })
        })

        viewModel.popularLiveData.observe(this, Observer { response ->
            handleDataDefault(response,
                onSuccess = {
                    handleData(it?.results)
                })
        })

        viewModel.loading.observe(this, Observer {
            if (it == false) {
                binding.recyclerView.refreshComplete()
                binding.recyclerView.loadMoreComplete()
            }
        })
    }

    private fun initView() {
        binding.recyclerView.apply {
            layoutManager = GridLayoutManager(context, 2, LinearLayoutManager.VERTICAL, false)
            adapter = this@DiscoverFragment.adapter
            setRefreshProgressStyle(ProgressStyle.BallClipRotateMultiple)
            setLoadingMoreProgressStyle(ProgressStyle.CubeTransition)


            setLoadingListener(object : XRecyclerView.LoadingListener {
                override fun onLoadMore() {
                    page++
                    fetchData(headerView.checkedChipId)
                }

                override fun onRefresh() {
                    page = 1
                    fetchData(headerView.checkedChipId)
                }
            })

            addHeaderView(headerView)
        }

        headerView.check(R.id.movie_upcoming)
    }

    private fun handleData(movies: List<Movie?>?) {
        if (movies == null)
            return

        val dataMovies = movies.filter { it?.backdropPath != null && it.posterPath != null }

        if (adapter.data.count() > 0) {
            val lastPosition: Int = adapter.data.count()
            adapter.data.addAll(dataMovies)
            adapter.notifyItemRangeChanged(lastPosition, dataMovies.count() )
        } else {
            adapter.data.addAll(dataMovies)
            adapter.notifyDataSetChanged()
        }

    }

    private fun fetchData(@IdRes chipId: Int) {
        when (chipId) {
            R.id.movie_upcoming -> viewModel.getMoviesPopular(MoviesByCategoryRequest((page)))
            R.id.movie_for_you -> {
            }
            R.id.movie_documentary -> viewModel.getMoviesByGenre(GenresID.Documentary.id, page)
            R.id.movie_comedy -> viewModel.getMoviesByGenre(GenresID.Comedy.id, page)
            R.id.movie_action -> viewModel.getMoviesByGenre(GenresID.Action.id, page)
            R.id.movie_thriller -> viewModel.getMoviesByGenre(GenresID.Thriller.id, page)
            R.id.movie_horror -> viewModel.getMoviesByGenre(GenresID.Horror.id, page)
            R.id.movie_science -> viewModel.getMoviesByGenre(GenresID.Fiction.id, page)
            R.id.movie_fantasy -> viewModel.getMoviesByGenre(GenresID.Fantasy.id, page)
            R.id.movie_movie -> viewModel.getMoviesByGenre(null, page)
            R.id.movie_tv_show -> {
            }
        }
    }
}