package com.huutho.movies.main

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.huutho.baselibrary.BaseViewModel
import com.huutho.baselibrary.data.Resource
import com.huutho.baselibrary.data.callApiDefault
import com.huutho.movies.MovieDBService
import com.huutho.movies.entity.DBConfiguration

class MainVM(private val movieDBService: MovieDBService) : BaseViewModel() {

    val loadingLiveData = MutableLiveData(false)

    val dbConfigurationLiveData = MutableLiveData<Resource<DBConfiguration>>()

    fun getMovieDBConfiguration() {
        viewModelScope.callApiDefault(dbConfigurationLiveData, loadingLiveData,
            onSuccess = {

            },
            dataSource = {
                movieDBService.getConfigurationDB()
            }
        )
    }
}