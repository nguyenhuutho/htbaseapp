package com.huutho.movies.main.adapter

import androidx.databinding.ViewDataBinding
import androidx.databinding.library.baseAdapters.BR
import com.huutho.baselibrary.recyclerview.adapter.BaseRvAdapter
import com.huutho.baselibrary.recyclerview.adapter.BaseViewHolderBinding
import com.huutho.movies.R
import com.huutho.movies.entity.Movie

class MoviesAdapter(private val onItemClick: (data: Movie?, position: Int) -> Unit) : BaseRvAdapter<Movie, MoviesAdapter.MovieMainVH>() {
    override fun getLayoutId(viewType: Int): Int = R.layout.item_movie_main

    override fun createViewHolder(viewDataBinding: ViewDataBinding, viewType: Int): MovieMainVH = MovieMainVH(viewDataBinding)

    override fun onBindVH(holder: MovieMainVH, position: Int) = holder.onBindView(data[position], position)

    inner class MovieMainVH(private val bindingView: ViewDataBinding) : BaseViewHolderBinding<Movie>(bindingView) {
        override fun onBindView(data: Movie?, position: Int) {
            setOnItemClick(onItemClick , data, position)
            bindingView.setVariable(BR.movie, data)
            bindingView.executePendingBindings()
        }
    }
}

