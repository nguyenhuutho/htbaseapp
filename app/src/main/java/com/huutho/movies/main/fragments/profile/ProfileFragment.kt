package com.huutho.movies.main.fragments.profile

import android.view.View
import com.huutho.baselibrary.BaseFragment
import com.huutho.movies.R
import com.huutho.movies.databinding.FragmentProfileBinding
import org.koin.androidx.viewmodel.ext.android.viewModel

class ProfileFragment : BaseFragment<FragmentProfileBinding, ProfileVM>() {

    override val viewModel: ProfileVM by viewModel()

    override fun getLayoutId(): Int = R.layout.fragment_profile

    override fun setBindingForLayout(binding: FragmentProfileBinding) {
        binding.vm = viewModel
    }

    override fun onViewReady(view: View) {

    }

    override fun onObserverListener(viewModel: ProfileVM) {

    }


}