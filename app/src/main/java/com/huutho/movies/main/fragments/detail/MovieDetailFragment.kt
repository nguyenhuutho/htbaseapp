package com.huutho.movies.main.fragments.detail

import android.view.View
import androidx.lifecycle.Observer
import com.huutho.baselibrary.BaseFragment
import com.huutho.baselibrary.data.handleDataDefault
import com.huutho.movies.R
import com.huutho.movies.databinding.FragmentMovieDetailBinding
import org.koin.androidx.viewmodel.ext.android.viewModel

class MovieDetailFragment : BaseFragment<FragmentMovieDetailBinding, MovieDetailVM>() {

    companion object {
        const val KEY_MOVIE_ID = "KEY_MOVIE_ID"
    }

    private val movieId by lazy { arguments?.getInt(KEY_MOVIE_ID) ?: -1 }

    override val viewModel: MovieDetailVM by viewModel()

    override fun getLayoutId(): Int = R.layout.fragment_movie_detail

    override fun setBindingForLayout(binding: FragmentMovieDetailBinding) {
        binding.vm = viewModel
    }

    override fun onViewReady(view: View) {
        viewModel.movieIdLV.postValue(movieId)
    }

    override fun onObserverListener(viewModel: MovieDetailVM) {
        viewModel.movieDetail.observe(this, Observer {
            handleDataDefault(it,
                onSuccess = {},
                onFailure = {}
            )
        })
    }
}