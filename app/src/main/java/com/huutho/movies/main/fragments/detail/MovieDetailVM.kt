package com.huutho.movies.main.fragments.detail

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.liveData
import androidx.lifecycle.switchMap
import com.huutho.baselibrary.BaseViewModel
import com.huutho.baselibrary.data.Resource
import com.huutho.baselibrary.data.emitDefault
import com.huutho.movies.MovieDBService
import com.huutho.movies.entity.MovieDetailResponse

class MovieDetailVM(private val movieDBService: MovieDBService) : BaseViewModel() {

    val movieIdLV = MutableLiveData<Int>()

    val movieDetail = movieIdLV.switchMap { movieId ->
        liveData<Resource<MovieDetailResponse>> {
            emitDefault(movieDBService.getMovieDetail(movieId))
        }
    }
}
