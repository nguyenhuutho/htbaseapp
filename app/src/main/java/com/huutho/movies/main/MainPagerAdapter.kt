package com.huutho.movies.main

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.huutho.movies.R
import com.huutho.movies.main.fragments.collections.CollectionFragment
import com.huutho.movies.main.fragments.discovers.DiscoverFragment
import com.huutho.movies.main.fragments.profile.ProfileFragment

class MainPagerAdapter(private val context: Context, fm: FragmentManager) :
    FragmentPagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    private val fragments = listOf(
        DiscoverFragment(),
        CollectionFragment(),
        ProfileFragment()
    )

    private val fragmentTitles = context.resources.getStringArray(R.array.fragments)

    override fun getItem(position: Int): Fragment = fragments[position]

    override fun getCount(): Int = fragments.count()

    override fun getPageTitle(position: Int): CharSequence? {
        return fragmentTitles[position]
    }
}