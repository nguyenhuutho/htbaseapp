package com.huutho.movies.main.fragments.collections

import android.view.View
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearSnapHelper
import androidx.recyclerview.widget.RecyclerView
import com.huutho.baselibrary.BaseFragment
import com.huutho.baselibrary.data.Resource
import com.huutho.baselibrary.data.handleDataDefault
import com.huutho.baselibrary.withArgs
import com.huutho.movies.R
import com.huutho.movies.databinding.FragmentCollectionBinding
import com.huutho.movies.entity.Movie
import com.huutho.movies.entity.MoviesByCategoryResponse
import com.huutho.movies.main.adapter.MoviesAdapter
import com.huutho.movies.main.fragments.detail.MovieDetailFragment
import com.huutho.utilslib.addFragment
import org.koin.androidx.viewmodel.ext.android.viewModel

class CollectionFragment : BaseFragment<FragmentCollectionBinding, CollectionVM>() {
    override val viewModel: CollectionVM by viewModel()

    override fun getLayoutId(): Int = R.layout.fragment_collection

    override fun setBindingForLayout(binding: FragmentCollectionBinding) {
        binding.vm = viewModel
    }

    override fun onViewReady(view: View) {
        viewModel.onRefreshDataMainScreen()
    }

    override fun onObserverListener(viewModel: CollectionVM) {
        handleCategoryCommon(viewModel.popularLiveData, binding.rvPopular)
        handleCategoryCommon(viewModel.upcomingLiveData, binding.rvUpcoming)
        handleCategoryCommon(viewModel.nowPlayingLiveData, binding.rvNowPlaying)
        handleCategoryCommon(viewModel.topRateLiveData, binding.rvTopRate)
        handleCategoryCommon(viewModel.trendingLiveData, binding.rvTrends)
    }

    private fun handleCategoryCommon(
        liveData: MutableLiveData<Resource<MoviesByCategoryResponse>>,
        recyclerView: RecyclerView
    ) {
        val adapter = MoviesAdapter { data: Movie?, _: Int ->
            val fragment = MovieDetailFragment()
                .withArgs { putInt(MovieDetailFragment.KEY_MOVIE_ID, data?.id ?: -1) }
            addFragment(R.id.root, fragment, fromBottom = false, isAddToBackStack = true)
        }
        LinearSnapHelper().attachToRecyclerView(recyclerView)
        recyclerView.layoutManager =
            LinearLayoutManager(this.context, LinearLayoutManager.HORIZONTAL, false)
        recyclerView.adapter = adapter
        liveData.observe(this, Observer {
            handleDataDefault(it,
                onSuccess = { response ->
                    adapter.data.clear()
                    adapter.data.addAll(response?.results ?: listOf())
                    adapter.notifyDataSetChanged()
                }
            )
        })
    }
}