package com.huutho.movies.main

import androidx.viewpager.widget.ViewPager
import com.huutho.baselibrary.BaseActivity
import com.huutho.movies.R
import com.huutho.movies.databinding.ActivityMainBinding
import org.koin.androidx.viewmodel.ext.android.viewModel


class MainActivity : BaseActivity<ActivityMainBinding, MainVM>() {

    override val viewModel: MainVM by viewModel()

    override fun getLayoutId(): Int = R.layout.activity_main

    override fun setBindingForLayout(binding: ActivityMainBinding) {
        binding.vm = viewModel
    }

    override fun onViewReady() {
        binding.viewPager.adapter = MainPagerAdapter(this, supportFragmentManager)
        binding.viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {

            }

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

            }

            override fun onPageSelected(position: Int) {
                binding.bottomNavView.selectedItemId = when (position) {
                    0 -> R.id.discover
                    1 -> R.id.collection
                    else -> R.id.profile
                }
            }
        })

        binding.bottomNavView.setOnNavigationItemSelectedListener { menuItem ->
            when (menuItem.itemId) {
                R.id.discover -> binding.viewPager.currentItem = 0
                R.id.collection -> binding.viewPager.currentItem = 1
                R.id.profile -> binding.viewPager.currentItem = 2

            }
            true
        }
    }

    override fun onObserverListener(viewModel: MainVM) {

    }
}

