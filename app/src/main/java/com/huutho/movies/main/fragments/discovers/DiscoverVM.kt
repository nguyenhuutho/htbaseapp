package com.huutho.movies.main.fragments.discovers

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.huutho.baselibrary.BaseViewModel
import com.huutho.baselibrary.data.Resource
import com.huutho.baselibrary.data.callApiDefault
import com.huutho.movies.MovieDBService
import com.huutho.movies.entity.MoviesByCategoryRequest
import com.huutho.movies.entity.MoviesByCategoryResponse

class DiscoverVM(private val movieDBService: MovieDBService) : BaseViewModel() {

    val loading = MutableLiveData<Boolean>()

    val popularLiveData = MutableLiveData<Resource<MoviesByCategoryResponse>>()

    val moviesByGenreId = MutableLiveData<Resource<MoviesByCategoryResponse>>()

    fun getMoviesPopular(req: MoviesByCategoryRequest) {
        viewModelScope.callApiDefault(popularLiveData, loading) {
            movieDBService.getMoviesPopular(req.page, req.region)
        }
    }

    fun getMoviesByGenre(genreId: Int?, page: Int) {
        viewModelScope.callApiDefault(popularLiveData, loading) {
            movieDBService.getMoVieByDiscover(withGenreId = genreId, page = page)
        }
    }
}