package com.huutho.movies.main.fragments.collections

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.huutho.baselibrary.BaseViewModel
import com.huutho.baselibrary.data.Resource
import com.huutho.baselibrary.data.callApiDefault
import com.huutho.movies.MovieDBService
import com.huutho.movies.entity.DBConfiguration
import com.huutho.movies.entity.MoviesByCategoryRequest
import com.huutho.movies.entity.MoviesByCategoryResponse

class CollectionVM(private val movieDBService: MovieDBService) : BaseViewModel() {
    val loadingLiveData = MutableLiveData(false)

    val dbConfigurationLiveData = MutableLiveData<Resource<DBConfiguration>>()
    val popularLiveData = MutableLiveData<Resource<MoviesByCategoryResponse>>()
    val upcomingLiveData = MutableLiveData<Resource<MoviesByCategoryResponse>>()
    val nowPlayingLiveData = MutableLiveData<Resource<MoviesByCategoryResponse>>()
    val topRateLiveData = MutableLiveData<Resource<MoviesByCategoryResponse>>()
    val trendingLiveData = MutableLiveData<Resource<MoviesByCategoryResponse>>()

    fun getMovieDBConfiguration() {
        viewModelScope.callApiDefault(dbConfigurationLiveData, loadingLiveData,
            onSuccess = {

            },
            dataSource = {
                movieDBService.getConfigurationDB()
            }
        )
    }

    fun getMoviesPopular(req: MoviesByCategoryRequest) {
        viewModelScope.callApiDefault(popularLiveData, loadingLiveData) {
            movieDBService.getMoviesPopular(req.page, req.region)
        }
    }

    fun getMoviesUpcoming(req: MoviesByCategoryRequest) {
        viewModelScope.callApiDefault(upcomingLiveData, loadingLiveData) {
            movieDBService.getMoviesUpcoming(req.page, req.region)
        }
    }

    fun getMoviesNowPlaying(req: MoviesByCategoryRequest) {
        viewModelScope.callApiDefault(nowPlayingLiveData, loadingLiveData) {
            movieDBService.getMoviesNowPlaying(req.page, req.region)
        }
    }

    fun getMoviesTopRate(req: MoviesByCategoryRequest) {
        viewModelScope.callApiDefault(topRateLiveData, loadingLiveData) {
            movieDBService.getMoviesTopRate(req.page, req.region)
        }
    }

    fun getMoviesTrending() {
        viewModelScope.callApiDefault(trendingLiveData, loadingLiveData) {
            movieDBService.getMoviesTrending()
        }
    }


    fun onRefreshDataMainScreen() {
        getMoviesPopular(MoviesByCategoryRequest(1))
        getMoviesUpcoming(MoviesByCategoryRequest(1))
        getMoviesNowPlaying(MoviesByCategoryRequest(1))
        getMoviesTopRate(MoviesByCategoryRequest(1))
        getMoviesTrending()
    }
}