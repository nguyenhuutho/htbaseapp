package com.huutho.movies

import com.huutho.baselibrary.BaseApplication
import com.huutho.baselibrary.network.RetrofitHelper
import com.huutho.movies.main.MainVM
import com.huutho.movies.main.fragments.collections.CollectionVM
import com.huutho.movies.main.fragments.detail.MovieDetailVM
import com.huutho.movies.main.fragments.discovers.DiscoverVM
import com.huutho.movies.main.fragments.profile.ProfileVM
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.module.Module
import org.koin.dsl.module

class MovieApplication : BaseApplication() {

    override val appModule: Module = module {
        single { AppSharePref(get(), BuildConfig.SHARE_PREF, get()) }
    }

    override val networkModule: Module = module {
        single {
            RetrofitHelper.initial(
                gson = get(),
                context = get(),
                baseUrl = BuildConfig.BASE_URL,
                serviceClazz = MovieDBService::class.java,
                queryParams = mapOf(
                    Pair(BuildConfig.API_PARAM_LANGUAGE, "en-US"),
                    Pair(BuildConfig.API_PARAM, BuildConfig.API_KEY)
                )
            )
        }
    }

    override val viewModelModule = module {
        viewModel { MainVM(get()) }
        viewModel { MovieDetailVM(get()) }
        viewModel { CollectionVM(get()) }
        viewModel { DiscoverVM(get()) }
        viewModel { ProfileVM(get()) }
    }
}