package com.huutho.movies

import com.huutho.movies.entity.DBConfiguration
import com.huutho.movies.entity.MovieDetailResponse
import com.huutho.movies.entity.MoviesByCategoryResponse
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface MovieDBService {

    @GET("configuration")
    suspend fun getConfigurationDB(): DBConfiguration

    @GET("movie/popular")
    suspend fun getMoviesPopular(@Query("page") page: Int?, @Query("region") region: String?): MoviesByCategoryResponse

    @GET("movie/upcoming")
    suspend fun getMoviesUpcoming(@Query("page") page: Int?, @Query("region") region: String?): MoviesByCategoryResponse

    @GET("movie/now_playing")
    suspend fun getMoviesNowPlaying(@Query("page") page: Int?, @Query("region") region: String?): MoviesByCategoryResponse

    @GET("movie/top_rated")
    suspend fun getMoviesTopRate(@Query("page") page: Int?, @Query("region") region: String?): MoviesByCategoryResponse

    @GET("trending/movie/week")
    suspend fun getMoviesTrending(): MoviesByCategoryResponse

    @GET("movie/{movie_id}?append_to_response=videos,images,genre,credits")
    suspend fun getMovieDetail(@Path("movie_id") movieId: Int): MovieDetailResponse

    @GET("discover/movie?sort_by=popularity.desc&include_adult=true&page=1")
    suspend fun getMoVieByDiscover(@Query("with_genres") withGenreId: Int?, @Query("page") page: Int = 1): MoviesByCategoryResponse
}