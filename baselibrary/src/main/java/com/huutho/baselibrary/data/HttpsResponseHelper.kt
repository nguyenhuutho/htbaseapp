package com.huutho.baselibrary.data

import androidx.lifecycle.LiveDataScope
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

data class Resource<out T : Entity>(val status: Status, val data: T?, val e: Exception?) {
    companion object {
        fun loading() = Resource(Status.LOADING, null, null)

        fun <T : Entity> success(data: T): Resource<T> = Resource(Status.SUCCESS, data, null)

        fun failure(e: Exception) = Resource(Status.FAILURE, null, e)

        fun complete() = Resource(Status.COMPLETE, null, null)
    }
}


suspend fun <T : Entity> execute(source: T): Resource<T> {
    return try {
        Resource.success(source)
    } catch (e: Exception) {
        e.printStackTrace()
        Resource.failure(e)
    }
}


suspend fun <T : Entity> LiveDataScope<Resource<T>>.emitDefault(source: T) {
    emit(Resource.loading())
    emit(execute(source))
    emit(Resource.complete())
}


fun <T : Entity> handleDataDefault(
    dataResponse: Resource<T>,
    onLoading: () -> Unit = {},
    onSuccess: (data: T?) -> Unit = {},
    onFailure: (ex: Exception?) -> Unit = {},
    onComplete: () -> Unit = {}
) {
    when (dataResponse.status) {
        Status.LOADING -> onLoading.invoke()
        Status.SUCCESS -> onSuccess.invoke(dataResponse.data)
        Status.FAILURE -> onFailure.invoke(dataResponse.e)
        Status.COMPLETE -> onComplete.invoke()
    }
}


fun <T : Entity> CoroutineScope.callApiDefault(
    originLiveData: MutableLiveData<Resource<T>>,
    loadingLiveData: MutableLiveData<Boolean> = MutableLiveData(false),
    onSuccess: (data: T) -> Unit = {},
    onFailure: (e: Exception) -> Unit = {},
    onComplete: () -> Unit = {},
    dataSource: suspend () -> T
) = launch {
    loadingLiveData.value = true
    originLiveData.value = Resource.loading()
    originLiveData.value = try {
        val data = dataSource.invoke()
        onSuccess.invoke(data)
        Resource.success(data)
    } catch (e: Exception) {
        e.printStackTrace()
        onFailure.invoke(e)
        Resource.failure(e)
    }
    originLiveData.value = Resource.complete()
    loadingLiveData.value = false
    onComplete.invoke()
}