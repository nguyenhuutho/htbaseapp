package com.huutho.baselibrary.recyclerview.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView

abstract class BaseRvAdapter<E, VH : RecyclerView.ViewHolder> : RecyclerView.Adapter<VH>() {

    val data: MutableList<E?> = mutableListOf()

    /**
     * Return layoutId resource for item
     */
    abstract fun getLayoutId(viewType: Int): Int

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        val layoutInflater = LayoutInflater.from(parent.context)
        val viewDataBinding = DataBindingUtil.inflate<ViewDataBinding>(
            layoutInflater,
            getLayoutId(viewType),
            parent,
            false
        )
        return createViewHolder(viewDataBinding, viewType)
    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        onBindVH(holder, position)
    }

    abstract fun createViewHolder(viewDataBinding: ViewDataBinding, viewType: Int): VH

    abstract fun onBindVH(holder: VH, position: Int)

    override fun getItemCount(): Int = data.count()

    fun clear() {
        data.clear()
        notifyDataSetChanged()
    }

    fun remove(index: Int, notifyDataSetChangedNow: Boolean = true) {
        if (index > -1 && index < data.count()) {
            data.removeAt(index)
            if (notifyDataSetChangedNow)
                notifyItemRemoved(index)
        }
    }

    fun remove(entity: E, notifyDataSetChangedNow: Boolean = true) {
        val indexOfE = data.indexOf(entity)
        if (indexOfE != -1) {
            data.remove(entity)
            if (notifyDataSetChangedNow)
                notifyItemRemoved(indexOfE)
        }
    }

    fun add(entity: E, notifyDataSetChangedNow: Boolean = true) {
        data.add(entity)
        if (notifyDataSetChangedNow)
            notifyItemInserted(data.count() - 1)
    }

    fun add(entity: E, index: Int, notifyDataSetChangedNow: Boolean = true) {
        data.add(index, entity)
        if (notifyDataSetChangedNow)
            notifyItemInserted(index)
    }

    fun replace(entity: E, index: Int, notifyDataSetChangedNow: Boolean = true) {
        data[index] = entity
        if (notifyDataSetChangedNow)
            notifyItemChanged(index)
    }

    fun getItem(index: Int): E? {
        return data[index]
    }

    fun findIndexOf(entity: E): Int {
        return data.indexOf(entity)
    }
}