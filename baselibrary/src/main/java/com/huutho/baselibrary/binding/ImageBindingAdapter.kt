package com.huutho.baselibrary.binding

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide

@BindingAdapter("app:imageSource")
fun imageSource(view: ImageView, url: String) {
    Glide.with(view.context).load(url).into(view)
}